@extends('layouts.base')

@section('content')
	<div class="container">
		<div class="col-sm-offset-2 col-sm-8">
			<div class="panel panel-default">
				<div class="panel-heading">
					新しいキャラクター
					<form action="/trncharacters/create" method="GET">
						{{ csrf_field() }}
						<button type="submit" class="btn btn-danger">
							＋追加
						</button>
					</form>
				</div>
			</div>

			<!-- Books -->
			@if (count($trnCharacters) > 0)
				<div class="panel panel-default">
					<div class="panel-heading">
						キャラクターデータ
					</div>

					<div class="panel-body">
						<table class="table table-striped task-table">
							<thead>
								<th>名前</th>
								<th>&nbsp;</th>
							</thead>
							<tbody>
								@foreach ($trnCharacters as $trnCharacter)
									<tr>
										<td><a href="/trncharacters/{{ $trnCharacter->id }}/edit">{{ $trnCharacter->name }}</a></td>
										<!-- Task Delete Button -->
										<td>
											<form action="/trncharacters/{{ $trnCharacter->id }}" method="POST">
												{{ csrf_field() }}
												{{ method_field('DELETE') }}

												<button type="submit" class="btn btn-danger">
													<i class="fa fa-trash"></i>削除
												</button>
											</form>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			@endif
		</div>
	</div>
@endsection
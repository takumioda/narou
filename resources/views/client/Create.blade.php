@extends('layouts.base')

@section('content')
	<div class="container">
		<div class="col-sm-offset-2 col-sm-8">
			<div class="panel panel-default">
				<div class="panel-heading">
					新しいキャラクター
				</div>

				<div class="panel-body">
					<!-- Display Validation Errors -->
					@include('common.errors')

					<!-- New Characters Form -->
					<form action="/mstClients" method="POST" class="form-horizontal">
						{{ csrf_field() }}

						<!-- mstClient Name -->
						<div class="form-group">
							<label for="task-name" class="col-sm-3 control-label">クライアントID</label>

							<div class="col-sm-6">
								<input type="text" name="clientid" id="clientid" class="form-control" value="{{ old('clientid') }}">
							</div>
						</div>
						<div class="form-group">
								<label for="task-name" class="col-sm-3 control-label">キャラクタ名</label>
	
								<div class="col-sm-6">
									<input type="text" name="name" id="book-name" class="form-control" value="{{ old('name') }}">
								</div>
						</div>
						<div class="form-group">
								<label for="task-name" class="col-sm-3 control-label">身長</label>
	
								<div class="col-sm-6">
									<input type="number" name="height" id="height" class="form-control" value="{{ old('height') }}">
								</div>
						</div>
						<div class="form-group">
								<label for="task-name" class="col-sm-3 control-label">体重</label>
	
								<div class="col-sm-6">
									<input type="number" name="weight" id="weight" class="form-control" value="{{ old('weight') }}">
								</div>
						</div>
						<div class="form-group">
								<label for="task-name" class="col-sm-3 control-label">出身地</label>
	
								<div class="col-sm-6">
									<input type="text" name="bornIn" id="bornIn" class="form-control" value="{{ old('bornIn') }}">
								</div>
						</div>
						<div class="form-group">
								<label for="task-name" class="col-sm-3 control-label">性格</label>
	
								<div class="col-sm-6">
									<input type="text" name="personality" id="personality" class="form-control" value="{{ old('personality') }}">
								</div>
						</div>
						<div class="form-group">
								<label for="task-name" class="col-sm-3 control-label">経験値取得倍率</label>
	
								<div class="col-sm-6">
									<input type="text" name="expRatio" id="expRatio" class="form-control" value="{{ old('expRatio') }}">
								</div>
						</div>
						<div class="form-group">
								<label for="task-name" class="col-sm-3 control-label">スキル取得倍率</label>
	
								<div class="col-sm-6">
									<input type="text" name="skillRatio" id="skillRatio" class="form-control" value="{{ old('skillRatio') }}">
								</div>
						</div>
						<div class="form-group">
								<label for="task-name" class="col-sm-3 control-label">魔法取得倍率</label>
	
								<div class="col-sm-6">
									<input type="text" name="magicRatio" id="magicRatio" class="form-control" value="{{ old('magicRatio') }}">
								</div>
						</div>

						<!-- Add Book Button -->
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-6">
								<button type="submit" class="btn btn-default">
									<i class="fa fa-plus"></i>追加する
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>			
		</div>
	</div>
@endsection
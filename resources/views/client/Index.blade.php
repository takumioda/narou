@extends('layouts.base')

@section('content')
	<div class="container">
		<div class="col-sm-offset-2 col-sm-8">
			<div class="panel panel-default">
				<div class="panel-heading">
					クライアント情報登録
					<form action="/client/create" method="GET">
						{{ csrf_field() }}
						<button type="submit" class="btn btn-danger">
							編集
						</button>
					</form>
				</div>
			</div>

			<!-- Books -->
			@if (count($mstClients) > 0)
				<div class="panel panel-default">
					<div class="panel-heading">
						キャラクターデータ
					</div>

					<div class="panel-body">
						<table class="table table-striped task-table">
							<thead>
								<th>名前</th>
								<th>&nbsp;</th>
							</thead>
							<tbody>
								@foreach ($mstClients as $mstClient)
									<tr>
										<td><a href="/mstClients/{{ $mstClient->id }}/edit">{{ $mstClient->name }}</a></td>
										<!-- Task Delete Button -->
										<td>
											<form action="/mstClients/{{ $mstClient->id }}" method="POST">
												{{ csrf_field() }}
												{{ method_field('DELETE') }}

												<button type="submit" class="btn btn-danger">
													<i class="fa fa-trash"></i>削除
												</button>
											</form>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			@endif
		</div>
	</div>
@endsection
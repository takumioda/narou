<?php

namespace App\Http\EloquentModel;

use Illuminate\Database\Eloquent\Model;

class Trn_Character extends Model
{
    protected $table = 'trn_characters';

    protected $guarded = array('id');

    public static $rules = array(
        'clientid' => 'required',
        'name' => 'required'
    );
}

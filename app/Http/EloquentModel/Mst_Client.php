<?php

namespace App\Http\EloquentModel;

use Illuminate\Database\Eloquent\Model;

class Mst_Client extends Model
{
    protected $table = 'mst_clients';

    protected $guarded = array('id');
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\EloquentModel\Trn_Character;

class TrnCharactersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $trnCharacters = Trn_Character::all();
        return view('chara.charaIndex',[
            'trnCharacters' => $trnCharacters
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $trnCharacters = Trn_Character::all();
        return view('chara.charaCreate',[
            'trnCharacters' => $trnCharacters
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this -> validate($request, Trn_Character::$rules);
        $trnCharacter = new Trn_Character;
        $form = $request -> all();
        unset($form['_token']);
        $trnCharacter -> fill($form) -> save();
        return redirect('/trncharacters');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $trnCharacters = Trn_Character::find($id);
        return view('chara.charaEdit',['form' => $trnCharacters]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this -> validate($request, Trn_Character::$rules);
        $trnCharacter = Trn_Character::find($id);
        $form = $request -> all();
        unset($form['_token']);
        $trnCharacter -> fill($form) -> save();
        return redirect('/trncharacters');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $trnCharacter = Trn_Character::destroy($id);
        return redirect('/trncharacters');
    }
}

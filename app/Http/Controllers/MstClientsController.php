<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MstClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mstClients = Mst_Client::all();
        return view('client.Index',[
            'mstClients' => $mstClients
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mstClients = Mst_Client::all();
        return view('client.Create',[
            'mstClients' => $mstClients
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this -> validate($request, Mst_Client::$rules);
        $mstClient = new Mst_Client;
        $form = $request -> all();
        unset($form['_token']);
        $mstClient -> fill($form) -> save();
        return redirect('/mstClients');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mstClients = Mst_Client::find($id);
        return view('client.Edit',['form' => $mstClients]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this -> validate($request, Mst_Client::$rules);
        $mstClient = Mst_Client::find($id);
        $form = $request -> all();
        unset($form['_token']);
        $mstClient -> fill($form) -> save();
        return redirect('/mstClients');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mstClient = Mst_Client::destroy($id);
        return redirect('/mstClients');
    }
}

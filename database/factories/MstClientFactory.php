<?php

use Faker\Generator as Faker;

$factory->define(App\Http\EloquentModel\Mst_Client::class, function (Faker $faker) {
    return [
        'userid' => $faker->userName,
        'passwd' => $faker->password,
        'fammilyName' => $faker->firstName,
        'lastName' => $faker->lastName,
        'birthDay' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'sex' => '男',
        'email' => $faker->safeEmail,
    ];
});

<?php

use Faker\Generator as Faker;

$factory->define(App\Http\EloquentModel\Trn_Character::class, function (Faker $faker) {
    return [
        'clientid' => $faker->numberBetween($min = 1, $max = 10),
        'name' => $faker->name,
        'height' => $faker->numberBetween($min = 30, $max = 500),
        'weight' => $faker->numberBetween($min = 1, $max = 1000),
        'bornIn' => $faker->country,
        'personality' => $faker->realText($faker->numberBetween(10,20)),
        'expRatio' => $faker->numberBetween($min = 0, $max = 500),
        'skillRatio' => $faker->numberBetween($min = 0, $max = 500),
        'magicRatio' => $faker->numberBetween($min = 0, $max = 500),
      ];
});

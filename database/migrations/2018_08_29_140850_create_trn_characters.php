<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrnCharacters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trn_characters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('clientid')->unsigned();            
            $table->string('name')->nullable();
            $table->integer('height')->nullable();
            $table->integer('weight')->nullable();
            $table->string('bornIn')->nullable();
            $table->string('personality')->nullable();
            $table->float('expRatio')->nullable();
            $table->float('skillRatio')->nullable();
            $table->float('magicRatio')->nullable();
            $table->timestamps();

            $table->foreign('clientid')
            ->references('id')->on('mst_clients')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trn_characters');
    }
}

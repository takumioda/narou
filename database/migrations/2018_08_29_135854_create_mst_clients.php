<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMstClients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('userid')->nullable();
            $table->string('passwd')->nullable();
            $table->string('fammilyName')->nullable();
            $table->string('lastName')->nullable();
            $table->date('birthDay')->nullable();
            $table->string('sex')->nullable();
            $table->string('email')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_clients');
    }
}

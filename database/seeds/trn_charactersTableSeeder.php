<?php

use Illuminate\Database\Seeder;

class trn_charactersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //fackerを使ったダミーデータ
        factory(App\Http\EloquentModel\Trn_Character::class, 100)->create();

        //直接定義
        $param = [
            'clientid' =>2,
            'name' =>'chara01',
            'height' =>'100',
            'weight' =>'100',
            'bornIn' =>'where',
            'personality' =>'test01',
            'expRatio' =>35,
            'skillRatio' =>35,
            'magicRatio' =>35,
        ];
        DB::table('trn_characters')->insert($param);
        $param = [
            'clientid' =>2,
            'name' =>'chara02',
            'height' =>'200',
            'weight' =>'200',
            'bornIn' =>'where2',
            'personality' =>'test02',
            'expRatio' =>40,
            'skillRatio' =>40,
            'magicRatio' =>40,
        ];
        DB::table('trn_characters')->insert($param);
        $param = [
            'clientid' =>2,
            'name' =>'chara03',
            'height' =>'300',
            'weight' =>'300',
            'bornIn' =>'where3',
            'personality' =>'test03',
            'expRatio' =>50,
            'skillRatio' =>50,
            'magicRatio' =>50,
        ];
        DB::table('trn_characters')->insert($param);
    }
}

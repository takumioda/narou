<?php

use Illuminate\Database\Seeder;

class mst_clientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //fackerを使ったダミーデータ
        factory(App\Http\EloquentModel\Mst_Client::class, 10)->create();

        //直接定義
        // $param = [
        //     'userid' => 'test',
        //     'passwd' => 'pass',
        //     'fammilyName'=> 'tarou',
        //     'lastName' => 'narou',
        //     'birthDay' => new DateTime('2000-04-08'),
        //     'sex' =>'男',
        //     'email' =>'animal@hamaguc.com',
        // ];
        // DB::table('mst_clients')->insert($param);

        // $param = [
        //     'userid' => 'hoge',
        //     'passwd' => 'hogepass',
        //     'fammilyName'=> 'takuya',
        //     'lastName' => 'hoge',
        //     'birthDay' => new DateTime('2002-08-08'),
        //     'sex' =>'男',
        //     'email' =>'hoge@hamaguc.com',
        // ];
        // DB::table('mst_clients')->insert($param);
    }
}
